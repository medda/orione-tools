<tool id="bam_to_consensus" name="BAM to consensus" version="1.0">
  <description>converts BAM to consensus sequence</description>
  <requirements>
    <requirement type="package" version="0.1.19">samtools</requirement>
  </requirements>
  <command>
    samtools mpileup 
    $anomalous_read_pairs
    -uf
    #if $source.index_source == "history"
      "${source.ref_file}"
    #else
      "${source.index.fields.path}"
    #end if
    "${source.input1}" 2>"${logfile}" | bcftools view -cg - 2>>"${logfile}" | tee "$fullvcf" | vcfutils.pl vcf2fq > "${out_fq}" &amp;&amp;
    vcfutils.pl varFilter "$fullvcf" > "${out_vcf}"
  </command>
  <inputs>
    <conditional name="source">
      <param name="index_source" type="select" label="Choose the source for the reference list">
        <option value="cached">Locally cached</option>
        <option value="history">History</option>
      </param>
      <when value="cached">
        <param name="input1" type="data" format="bam" metadata_name="dbkey" label="Input BAM file">
          <validator type="unspecified_build" />
          <validator type="dataset_metadata_in_data_table" table_name="sam_fa_indexes" metadata_name="dbkey" metadata_column="1" message="Sequences are not currently available for the specified build." />
        </param>
        <param name="index" type="select" label="Using reference genome">
          <options from_data_table="sam_fa_indexes">
            <filter type="data_meta" ref="input1" key="dbkey" column="1" />
            <validator type="no_options" message="No reference genome is available for the build associated with the selected input dataset" />
          </options>
        </param>
      </when>
      <when value="history">
        <param name="input1" type="data" format="bam" label="Input BAM file" />
        <param name="ref_file" type="data" format="fasta" metadata_name="dbkey" label="Using reference file" help="FASTA format" />
      </when>
    </conditional>
    <param name="anomalous_read_pairs" type="boolean" truevalue="-A" falsevalue="" checked="false" label="Count anomalous read pairs (-A)" />
  </inputs>
  <outputs>
    <data format="fastq" name="out_fq" label="${tool.name} on ${on_string}: FASTQ">
      <actions>
        <conditional name="source.index_source">
          <when value="cached">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="source.input1" param_attribute="dbkey" />
            </action>
          </when>
          <when value="history">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="source.ref_file" param_attribute="dbkey" />
            </action>
          </when>
        </conditional>
      </actions>
    </data>
    <data format="vcf" name="out_vcf" label="${tool.name} on ${on_string}: confident variants">
      <actions>
        <conditional name="source.index_source">
          <when value="cached">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="source.input1" param_attribute="dbkey" />
            </action>
          </when>
          <when value="history">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="source.ref_file" param_attribute="dbkey" />
            </action>
          </when>
        </conditional>
      </actions>
    </data>
    <data format="vcf" name="fullvcf" label="${tool.name} on ${on_string}: all variants">
      <actions>
        <conditional name="source.index_source">
          <when value="cached">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="source.input1" param_attribute="dbkey" />
            </action>
          </when>
          <when value="history">
            <action type="metadata" name="dbkey">
              <option type="from_param" name="source.ref_file" param_attribute="dbkey" />
            </action>
          </when>
        </conditional>
      </actions>
    </data>
    <data format="txt" name="logfile" label="${tool.name} on ${on_string}: log"/>
  </outputs>
  <tests>
    <test>
      <param name="index_source" value="history" /> 
      <param name="input1" value="gatk/gatk_table_recalibration/gatk_table_recalibration_out_1.bam" ftype="bam" />
      <param name="ref_file" value="phiX.fasta" ftype="fasta" />
      <output name="out_fq" file="bam_to_consensus.fastq" />
      <output name="out_vcf" file="bam_to_consensus.vcf" />
      <output name="fullvcf" file="bam_to_consensus_all.vcf" />
      <output name="logfile" file="bam_to_consensus.log" />
    </test>
  </tests>
  <help>

**What it does**

Given a BAM file and the corresponding reference genome, this tool constructs a consensus sequence for the given alignment file
and creates a file with the variant sites using `SAMtools`_ scripts.

The command line executed for the construction of the consensus sequence is::

 samtools mpileup -uf reference.fa aligment.bam | bcftools view -cg - | vcfutils.pl vcf2fq

The command line executed for the extraction of *all* variant sites is::

 samtools mpileup -uf reference.fa aligment.bam | bcftools view -cg -

The command line executed for the extraction of the *confident* variant sites is::

 samtools mpileup -uf reference.fa aligment.bam | bcftools view -cg - | vcfutils.pl varFilter
 
If *Count anomalous read pairs* is checked, mpileup is executed with the option -A.

------

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `SAMtools`_.

.. _SAMtools: http://samtools.sourceforge.net/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Li2009|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Li2009| replace:: Li, H., Handsaker, B., Wysoker, A., Fennell, T., Ruan, J., Homer, N., Marth, G., Abecasis, G., Durbin, R., 1000 Genome Project Data Processing Subgroup (2009) The Sequence Alignment/Map format and SAMtools. *Bioinformatics* 25(16), 2078-2079
.. _Li2009: http://bioinformatics.oxfordjournals.org/content/25/16/2078
  </help>
</tool>
