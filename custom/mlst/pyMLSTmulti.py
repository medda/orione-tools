# -*- coding: utf-8 -*-
import glob
import optparse
import os
import shutil
import subprocess
import sys
import urllib2

def revcompl(sequence):
    """ Reverse complement of a DNA sequence """
    dna_compl = {'A':'T', 'G':'C', 'T':'A', 'C':'G', 'N':'N'}
    revcompl_list = [dna_compl[el] for el in reversed(sequence)]
    return ''.join(revcompl_list)

def translate(sequence):
    """ Translate a DNA sequence into peptides -frame 1 """
    genetic_code = {'TTT':'F', 'TCT':'S', 'TAT':'Y', 'TGT':'C', \
                    'TTC':'F', 'TCC':'S', 'TAC':'Y', 'TGC':'C', \
                    'TTA':'L', 'TCA':'S', 'TAA':'*', 'TGA':'*', \
                    'TTG':'L', 'TCG':'S', 'TAG':'*', 'TGG':'W', \
                    'CTT':'L', 'CCT':'P', 'CAT':'H', 'CGT':'R', \
                    'CTC':'L', 'CCC':'P', 'CAC':'H', 'CGC':'R', \
                    'CTA':'L', 'CCA':'P', 'CAA':'Q', 'CGA':'R', \
                    'CTG':'L', 'CCG':'P', 'CAG':'Q', 'CGG':'R' , \
                    'ATT':'I', 'ACT':'T', 'AAT':'N', 'AGT':'S', \
                    'ATC':'I', 'ACC':'T', 'AAC':'N', 'AGC':'S', \
                    'ATA':'I', 'ACA':'T', 'AAA':'K', 'AGA':'R', \
                    'ATG':'M', 'ACG':'T', 'AAG':'K', 'AGG':'R', \
                    'GTT':'V', 'GCT':'A', 'GAT':'D', 'GGT':'G', \
                    'GTC':'V', 'GCC':'A', 'GAC':'D', 'GGC':'G', \
                    'GTA':'V', 'GCA':'A', 'GAA':'E', 'GGA':'G' , \
                    'GTG':'V', 'GCG':'A', 'GAG':'E', 'GGG':'G'}  
    initiators = ['GTG', 'ATG', 'CTG', 'TTG', 'ATT', 'ATC', 'ATA'] 
    codons = []
    triplet = []
    for nucleotide in sequence.upper():
        triplet.append(nucleotide)
        if len(triplet) == 3:
            codons.append("".join(triplet))
            triplet = []
    if triplet != []: codons.append("".join(triplet))
    #peptide = [genetic_code[codon] for codon in codons]
    peptide = []
    for codon in codons:
        if codon in genetic_code: peptide.append(genetic_code[codon])
        else: peptide.append('*')
    if codons[0] in initiators: peptide[0] = 'M'
    return ''.join(peptide)

class FastaStream:

	def __init__(self, multifastafile):
		"""
		Generate a stream of 'FASTA strings' from an io stream.
		"""
		self.infile = open(multifastafile)
		self.buffer = []

	def __del__(self) :
		self.infile.close()

	def __iter__(self):
		return self

	def next(self):
		while 1:
			try:
				line = self.infile.next()
				if line.startswith('>') and self.buffer:
					fasta = "".join(self.buffer)
					self.buffer = [line]
					return fasta
				else:
					self.buffer.append(line)
			except StopIteration:
				if self.buffer:
					fasta = "".join(self.buffer)
					self.buffer = []
					return fasta
				else:
					raise StopIteration

# end of class

class MultiLocus(object):

    def __init__(self, draft, referencename, coordfile, extractedseq, extractedcds, extractedpep, aligned, cdsaligned, protaligned, summarytab):
        self.draft = draft
        self.referencename = referencename
        self.coordfile = coordfile
        self.coords = {}
        self.genefilenames = []
        self.cdsfilenames = []
        self.extractedseq = extractedseq
        self.extractedcds = extractedcds
        self.extractedpep = extractedpep
        self.aligned = aligned
        self.cdsaligned = cdsaligned
        self.protaligned = protaligned
        self.summarytab = summarytab
        self.lengthdict = {}

    def run(self):
        """ """
        self._fetchReferenceSeq()
        self._acquireReferenceCoords()
        self._extractReferenceSeq()
        self._extractTargetOrfs()
        self._extractTargetPeptides()
        for geneName in self.genefilenames:
            self._analyzeGene(geneName)
            self._analyzeCds(geneName)
            self._analyzePept(geneName)            
        self._setTab()
        self._cleanEnvironment()

    def _setTab(self):
        """ """
        with open(self.summarytab, 'w') as out:
            out.write("gene\tgenLen\taligned\tident\tmismatch\tcov\tcdsLen\tteorProtLen\talign\tprotid\tmismatch\n")
            for geneName in self.coords:
                fields = self.lengthdict[geneName]
                try:
                    genecov = round((float(fields[2]) / fields[0] * 100), 1)
                    line = [geneName, str(fields[0]), str(fields[2]), str(fields[3]), str(fields[4]), str(genecov), str(fields[1]), str(int(float(fields[1])/3)) , str(fields[5]), str(fields[6]), str(fields[7])]
                except:
                    line = [geneName, str(fields[0]), "0", "0", "0", "0", str(fields[1]), str(int(float(fields[1])/3)), "0", "0", "0"]
                out.write("\t".join(line)+ "\n")

    def _fetchReferenceSeq(self):
        """ """
        with open(self.referencename, 'r') as refFasta:
            handle = refFasta.read()
            self.refSequ = "".join(handle.split("\n")[1:])
            self.refTitle = handle.split("\n")[0].split(" ")[0].replace(">", "")

    def _acquireReferenceCoords(self):
        """ """
        with open(self.coordfile) as cf:
            for num, line in enumerate(cf, 1):
                cols = line.split("\t")
                if len(cols) < 3 or len(cols) > 4:
                    raise ValueError("Line %d '%s' does not contain 3-4 columns" % (num, line.rstrip("\n")))
                geneName = cols[0].strip()
                start = cols[1].split('-')[0].strip().replace('c', '')
                stop = cols[1].split('-')[1].strip()
                cdsstart = cols[2].split('-')[0].strip().replace('c', '')
                cdsstop = cols[2].split('-')[1].strip()
                note = cols[3].strip() if len(cols) == 4 else ''
                strand = 'neg' if 'c' in cols[1].strip() else 'pos'
                self.coords[geneName] = (int(start), int(stop), strand, note, int(cdsstart), int(cdsstop))

    def _extractReferenceSeq(self):
        """ """
        for geneName in self.coords:
            start, stop, strand, note, cdsstart, cdsstop = self.coords[geneName]
            self.lengthdict[geneName] = [stop-start, cdsstop-cdsstart]
            
            if strand == "pos":
                subsequenceGene = self.refSequ[start -1:stop]
                subsequenceCds = self.refSequ[cdsstart -1:cdsstop]
                subsequencePept = translate(subsequenceCds)
                queryHeadGene = ">" + self.refTitle +"_"+ geneName + "_" + str(start) + "_" + str(stop) + " " + note
                queryHeadCds = ">" + self.refTitle +"_"+ geneName + "_cds" + str(cdsstart) + "_" + str(cdsstop) + " " + note
                queryHeadPept = ">" + self.refTitle +"_"+ geneName + "_pep" + str(cdsstart) + "_" + str(cdsstop) + " " + note
            else:
                subsequenceGene = revcompl(self.refSequ[stop -1:start])
                subsequenceCds = revcompl(self.refSequ[cdsstop -1:cdsstart])
                subsequencePept = translate(subsequenceCds)
                queryHeadGene = ">" + self.refTitle +"_"+ geneName + "_c" + str(start) + "_" + str(stop) + " " + note
                queryHeadCds = ">" + self.refTitle +"_"+ geneName + "_cds_c" + str(cdsstart) + "_" + str(cdsstop) + " " + note
                queryHeadPept = ">" + self.refTitle +"_"+ geneName + "_pep_c" + str(cdsstart) + "_" + str(cdsstop) + " " + note

            self._setOutQueryGene(geneName, queryHeadGene + "\n" + subsequenceGene + "\n")
            self._setOutQueryCds(geneName, queryHeadCds + "\n" + subsequenceCds + "\n")
            self._setOutQueryPep(geneName, queryHeadPept + "\n" + subsequencePept + "\n")

    def _setOutQueryGene(self, geneName, queryFasta):
        """ """
        self.genefilenames.append(geneName)
        with open(geneName + '.nt.fasta', 'w') as out:
            out.write(queryFasta)

    def _setOutQueryCds(self, geneName, queryFasta):
        """ """
        with open(geneName + '.cds.fasta', 'w') as out:
            out.write(queryFasta)

    def _setOutQueryPep(self, geneName, queryFasta):
        """ """
        with open(geneName + '.pep.fasta', 'w') as out:
            out.write(queryFasta)

    def _extractTargetOrfs(self):
        """ """
        command = "long-orfs -g 100 -l -z 11 %s %s" % (self.draft, self.draft + '.coords')
        subprocess.check_call(command, stderr=sys.stdout.fileno(), shell=True) # need to redirect stderr because long-orfs writes some logging info there. Cannot use stderr=subprocess.STDOUT if not assigning a value to stdout parameter
        command = "extract -l 100 %s %s > %s" % (self.draft, self.draft + '.coords', self.draft + '.orfs.fasta')
        subprocess.check_call(command, stderr=sys.stdout.fileno(), shell=True) # need to redirect stderr because long-orfs writes some logging info there. Cannot use stderr=subprocess.STDOUT if not assigning a value to stdout parameter

    def _extractTargetPeptides(self):
        """ """
        orfs = FastaStream(self.draft + '.orfs.fasta')
        with open(self.draft + '.pept.fasta', 'w') as out:
            for fasta in orfs:
                title = fasta.split('\n')[0]
                sequence = "".join(fasta.split('\n')[1:]).strip()
                out.write(title + '\n'+ translate(sequence) + '\n')

# blasting - extracting - aligning Genomic Regions
 
    def _analyzeGene(self, geneName):
        """ """
        self._blastGene(geneName)
        infile = open(geneName + ".nt.blast").readlines()
        if len(infile) >= 1:
            self._extractGene(geneName)
            self._alignGene(geneName)
        else:
            self.lengthdict[geneName] += ["0", "0", "0"]

    def _blastGene(self, geneName):
        """ """
        command = 'blastn -subject %s -query %s -out %s -evalue 0.01 -word_size 11 -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq" -max_target_seqs 1' % (self.draft, geneName+ ".nt.fasta", geneName + ".nt.blast")
        subprocess.check_call(command, shell=True)

    def _extractGene(self, geneName):
        """ """
        with open(geneName + '.nt.blast') as f:
            infile = f.readlines()
        for line in infile:
            cols = line.split("\t")
            query = '>' + cols[0] + "\n" + cols[12] + "\n"
            subject = '>' + cols[1] + '_' + cols[8] + '_' + cols[9] + "\n" + cols[13] + "\n"
            aligned = int(cols[9]) - int(cols[8]) #cols[3]
            if aligned < 0:
                aligned = aligned * -1
            identity = cols[2]
            mismatch = cols[4]
            self.lengthdict[geneName] += [aligned, identity, mismatch]
            self._setOutMultiFasta(geneName, query + subject)

    def _setOutMultiFasta(self, geneName, multifasta):
        """ """
        with open(geneName + '.nt.mfs', 'w') as out:
            out.write(multifasta)

    def _alignGene(self, geneName):
        """ """
        command = "clustalw2 -INFILE=%s -TYPE=DNA -OUTFILE=%s -OUTPUT=CLUSTAL" % (geneName + '.nt.mfs', geneName + '.nt.clw')
        subprocess.check_call(command, shell=True)

# blasting - extracting - aligning CDSs

    def _analyzeCds(self, geneName):
        """ """
        self._blastCds(geneName)
        infile = open(geneName + ".cds.tblastx").readlines()
        if len(infile) >= 1:
            self._extractCds(geneName)
            self._alignCds(geneName)
        else:
            self.lengthdict[geneName] += ["0", "0", "0"]

    def _blastCds(self, geneName):
        """ """
        command = 'tblastx -subject %s -query %s -out %s -evalue 0.01 -word_size 2 -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq" -max_target_seqs 1' % (self.draft + '.orfs.fasta', geneName+ ".cds.fasta", geneName + ".cds.tblastx")
        subprocess.check_call(command, shell=True)

    def _extractCds(self, geneName):
        """ """
        infile = open(geneName + ".cds.tblastx").readlines()
        for line in infile:
            cols = line.split("\t")
            query = '>' + cols[0] + "\n" + cols[12] + "\n"
            subject = '>' + cols[1] + '_' + cols[8] + '_' + cols[9] + "\n" + cols[13] + "\n"
            aligned = int(cols[9]) - int(cols[8]) #cols[3]
            if aligned < 0:
                aligned = aligned * -1
            identity = cols[2]
            mismatch = cols[4]
            self.lengthdict[geneName] += [aligned, identity, mismatch]
            self._setOutMultiCds(geneName, query + subject)

    def _setOutMultiCds(self, geneName, multifasta):
        """ """
        with open(geneName + '.cds.mfs', 'w') as out:
            out.write(multifasta)

    def _alignCds(self, geneName):
        """ """
        command =  "clustalw2 -INFILE=%s -TYPE=DNA -OUTFILE=%s -OUTPUT=CLUSTAL" % (geneName + '.cds.mfs', geneName + '.cds.clw')
        subprocess.check_call(command, shell=True)

# blasting - extracting - aligning Proteins

    def _analyzePept(self, geneName):
        """ """
        self._blastPept(geneName)
        infile = open(geneName + ".pep.blastp").readlines()
        if len(infile) >= 1:
            #infile = open(geneName + ".pep.blastp").readlines()
            self._extractPept(geneName)
            self._alignPept(geneName)
        else:
            self.lengthdict[geneName] += ["0", "0", "0"]

    def _blastPept(self, geneName):
        """ """
        command = 'blastp -subject %s -query %s -out %s -evalue 0.01 -word_size 2 -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq" -max_target_seqs 1' % (self.draft + '.pept.fasta', geneName+ ".pep.fasta", geneName + ".pep.blastp")
        subprocess.check_call(command, shell=True)

    def _extractPept(self, geneName):
        """ """
        infile = open(geneName + ".pep.blastp").readlines()
        #print geneName, "pept----", len(infile)
        for line in infile:
            cols = line.split("\t")
            query = '>' + cols[0] + "\n" + cols[12] + "\n"
            subject = '>' + cols[1] + '_' + cols[8] + '_' + cols[9] + "\n" + cols[13] + "\n"
            aligned = int(cols[9]) - int(cols[8]) #cols[3]
            if aligned < 0:
                aligned = aligned * -1
            identity = cols[2]
            mismatch = cols[4]
            self.lengthdict[geneName] += [aligned, identity, mismatch]
            self._setOutMultiPept(geneName, query + subject)

    def _setOutMultiPept(self, geneName, multifasta):
        """ """
        with open(geneName + '.pep.mfs', 'w') as out:
            out.write(multifasta)

    def _alignPept(self, geneName):
        """ """
        command =  "clustalw2 -INFILE=%s -TYPE=PROTEIN -OUTFILE=%s -OUTPUT=CLUSTAL" % (geneName + '.pep.mfs', geneName + '.pep.clw')
        subprocess.check_call(command, shell=True)

# cleaning

    def _cleanEnvironment(self):
        """ """
        # reference sequences
        with open(self.extractedseq, 'w') as destination:
            for geneName in self.genefilenames:
                try:
                    with open(geneName + '.nt.fasta', 'r') as source:
                        shutil.copyfileobj(source, destination)
                except:
                    continue                    
        with open(self.extractedcds, 'w') as destination:
            for geneName in self.genefilenames:
                try:
                    with open(geneName + '.cds.fasta', 'r') as source:
                        shutil.copyfileobj(source, destination)
                except:
                    continue                    
        with open(self.extractedpep, 'w') as destination:
            for geneName in self.genefilenames:
                try:
                    with open(geneName + '.pep.fasta', 'r') as source:
                        shutil.copyfileobj(source, destination) 
                except:
                    continue
        # alignments
        with open(self.aligned, 'w') as destination:
            for geneName in self.genefilenames:
                try:
                    with open(geneName + '.nt.clw', 'r') as source:
                        shutil.copyfileobj(source, destination)
                except:
                    continue                    
        with open(self.cdsaligned, 'w') as destination:
            for geneName in self.genefilenames:
                try:
                    with open(geneName + '.cds.clw', 'r') as source:
                        shutil.copyfileobj(source, destination)
                except:
                    continue                    
        with open(self.protaligned, 'w') as destination:
            for geneName in self.genefilenames:
                try:
                    with open(geneName + '.pep.clw', 'r') as source:
                        shutil.copyfileobj(source, destination) 
                except:
                    continue                    
        # cleaning
        for geneName in self.genefilenames:
            for f in glob.glob(geneName + '.*.dnd'):
                os.remove(f)
            for f in glob.glob(geneName + '.*.clw'):
                os.remove(f)
            for f in glob.glob(geneName + '.*.mfs'):
                os.remove(f)
            for f in glob.glob(geneName + '.*.fasta'):
                os.remove(f)
            os.remove(geneName + '.nt.blast')
            os.remove(geneName + '.cds.tblastx')
            os.remove(geneName + '.pep.blastp')
        os.remove(self.draft + '.coords')


def __main__():
    parser = optparse.OptionParser()
    parser.add_option('-d', dest='draftname', help='Draft genome')
    parser.add_option('-r', dest='referencename', help='Reference Genome')
    parser.add_option('-c', dest='coordfile', help='Coordinate file')
    parser.add_option('--es', dest='extractedseq', help='Genes of interest')
    parser.add_option('--ec', dest='extractedcds', help='Genes of interest')
    parser.add_option('--ep', dest='extractedpep', help='Genes of interest')
    parser.add_option('--al', dest='aligned', help='Gene alignments')
    parser.add_option('--cl', dest='cdsaligned', help='CDS alignments')
    parser.add_option('--pl', dest='protaligned', help='Gene alignments')
    parser.add_option('--st', dest='summarytab', help='Summary table')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    M = MultiLocus(options.draftname, options.referencename, options.coordfile, options.extractedseq, options.extractedcds, options.extractedpep, options.aligned, options.cdsaligned, options.protaligned, options.summarytab)
    M.run()

# python pyMLSTmulti.py -d Galaxy.draft.fasta -r Galaxy.ref.fasta -c Galaxy.test.tabular --es extractedseq.fasta --ec extractedcds.fasta --ep extractedpep.fasta --al alignedgene.clw --cl alignedcds.clw --pl alignedprot.clw --st table.tab 

if __name__ == "__main__":
    __main__()
