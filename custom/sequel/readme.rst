SEQuel wrapper
==============

Configuration
-------------

sequel_wrapper tool may be configured to use more than one CPU core by selecting an appropriate destination for this tool in Galaxy job_conf.xml file (see https://wiki.galaxyproject.org/Admin/Config/Jobs and https://wiki.galaxyproject.org/Admin/Config/Performance/Cluster ).

If you are using Galaxy release_2013.11.04 or later, this tool will automatically use the number of CPU cores allocated by the job runner according to the configuration of the destination selected for this tool.

If instead you are using an older Galaxy release, you should also add a line

  GALAXY_SLOTS=N; export GALAXY_SLOTS

(where N is the number of CPU cores allocated by the job runner for this tool) to the file

  <tool_dependencies_dir>/sequel/1.0.2/crs4/sequel/<hash_string>/env.sh

Version history
---------------

- Release 2: Fix version for blat requirement (reported by Björn Grüning). Upgrade BWA dependency to v. 0.7.7 . Update Orione citation.
- Release 1: Use $GALAXY_SLOTS instead of $SEQUEL_SITE_OPTIONS. Depend on package_blat_35x1 . Add readme.rst . Update Orione citation.
- Release 0: Initial release in the Tool Shed.

Development
-----------

Development is hosted at https://bitbucket.org/crs4/orione-tools . Contributions and bug reports are very welcome!
