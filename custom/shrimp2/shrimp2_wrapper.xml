<tool id="shrimp2_wrapper" name="SHRiMP2 for letter-space" version="1.0.0">
  <description>reads mapping against reference sequence</description>
  <requirements>
    <requirement type="package" version="2.2.3">shrimp</requirement>
  </requirements>
  <version_command>gmapper-ls --help 2&gt;&amp;1| sed -e 1d -e 4q</version_command>
  <command>
    gmapper-ls --sam --sam-unaligned
    --threads \${GALAXY_SLOTS:-1}
    ## Type of Reads
    #if $type_of_reads.input1.ext != 'fasta'
      --fastq
      --no-qv-check
      #if $type_of_reads.input1.ext == 'fastqsanger'
        --qv-offset 33 ## default is 64, i.e. fastqsolexa/fastqillumina datatypes
      #end if
    #end if
    #if $param.skip_or_full == 'full'
      #if $param.spaced_seed
        -s "${param.spaced_seed}"
      #end if
      #if str($param.max_hits_per_read)
        -o ${param.max_hits_per_read}
      #end if
      #if str($param.max_aln_per_read)
        --max-alignments ${param.max_aln_per_read}
      #end if
      #if str($param.seed_window_length)
        --match-window ${param.seed_window_length}%
      #end if
      #if str($param.sw_match_value)
        --match ${param.sw_match_value}
      #end if
      #if str($param.sw_mismatch_value)
        --mismatch ${param.sw_mismatch_value}
      #end if
      #if str($param.sw_gap_open_ref)
        --open-r ${param.sw_gap_open_ref}
      #end if
      #if str($param.sw_gap_open_query)
        --open-q ${param.sw_gap_open_query}
      #end if
      #if str($param.sw_gap_ext_ref)
        --ext-r ${param.sw_gap_ext_ref}
      #end if
      #if str($param.sw_gap_ext_query)
        --ext-q ${param.sw_gap_ext_query}
      #end if
      #if str($param.sw_hit_threshold)
        --full-threshold ${param.sw_hit_threshold}%
      #end if
      #if str($param.cmw_threshold)
        -r ${param.cmw_threshold}%
      #end if
      #if str($param.cmw_overlap)
        --cmw-overlap ${param.cmw_overlap}%
      #end if
      #if str($param.longest_read)
        --longest-read ${param.longest_read}
      #end if
      #if $param.insert_size_dist
        --insert-size-dist "${param.insert_size_dist}"
      #end if
      #if $param.read_group
        --read-group "${param.read_group}"
      #end if
      ${param.single_best_mapping}
      ${param.no_mapping_qualities}
      ${param.strata}
##      #if str($param.trim_front)
##        --trim-front ${param.trim_front}
##      #end if
##      #if str($param.trim_end)
##        --trim-end ${param.trim_end}
##      #end if
      #if str($param.min_avg_qv)
        --min-avg-qv ${param.min_avg_qv}
      #end if
      ${param.trim_off}
      ${param.ungapped}
      ${param.globalocal}
      ${param.strandmap}
    #end if
    #if $type_of_reads.single_or_paired == 'single'
      ${type_of_reads.input1}
    #else
      -p $type_of_reads.paired_mode
      #if $type_of_reads.insertion_size
        -I "${type_of_reads.insertion_size}"
      #end if
      -1 ${type_of_reads.input1}
      -2 ${type_of_reads.input2}
    #end if
    $input_target &gt;$output 2&gt;$log
  </command>
  <stdio>
    <exit_code range="1:" level="fatal" />
  </stdio>
  <inputs>
    <conditional name="type_of_reads">
      <param name="single_or_paired" type="select" label="Reads type">
        <option value="single">Single-end</option>
        <option value="paired">Paired-end</option>
      </param>
      <when value="single">
        <param name="input1" type="data" format="fasta,fastqsanger,fastqsolexa,fastqillumina" label="Single reads" />
      </when>
      <when value="paired">
        <param name="paired_mode" type="select" label="Paired mode">
          <option value="opp-in">Opposing strands, inwards</option>
          <option value="opp-out">Opposing strands, outwards</option>
          <option value="col-fw">Colinear, second is forward</option>
          <option value="col-bw">Colinear, second is backward</option>
        </param>
        <param name="insertion_size" type="text" size="9" value="0,1000" label="Insertion size range (-I)" help="Format: &quot;min,max&quot;" />
        <param name="input1" type="data" format="fasta,fastqsanger,fastqsolexa,fastqillumina" label="Forward reads (-1)" />
        <param name="input2" type="data" format="fasta,fastqsanger,fastqsolexa,fastqillumina" label="Reverse reads (-2)" />
      </when>
    </conditional>
    <param name="input_target" type="data" format="fasta" label="Target (reference) sequences" help="FASTA format" />
    <conditional name="param">
      <param name="skip_or_full" type="select" label="SHRiMP settings to use" help="For most mapping needs select &quot;Commonly used&quot;. If you want full control, select &quot;Full parameter list&quot;">
        <option value="skip">Commonly used</option>
        <option value="full">Full parameter list</option>
      </param>
      <when value="skip" />
      <when value="full">
        <param name="globalocal" type="select" display="radio" label="Perform full global / local alignment">
          <option value="--global">global</option>
          <option value="--local">local</option>
        </param>
        <param name="strandmap" type="select" display="radio" label="Strand Mapping Orientation">
          <option value="">both</option>
          <option value="--negative">negative strand (-C)</option>
          <option value="--positive">positive strand (-F)</option>
        </param>
        <param name="ungapped" type="boolean" truevalue="--ungapped" falsevalue="" checked="false" label="Perform ungapped alignment (--ungapped)" />
        <param name="spaced_seed" type="text" size="30" value="11110111101111,1111011100100001111,1111000011001101111" label="Spaced Seed (-s)" />
        <param name="max_hits_per_read" type="integer" value="10" optional="true" label="Maximum hits per read (-o)" />
        <param name="max_aln_per_read" type="integer" value="0" optional="true" label="Maximum number of alignments per read (--max-alignments)" help="0=all" />
        <param name="seed_window_length" type="float" value="140.0" optional="true" label="Seed window length (--match-window)" help="In percentage" />
        <param name="sw_match_value" type="integer" value="10" optional="true" label="S-W match score (--match)" />
        <param name="sw_mismatch_value" type="integer" value="-15" optional="true" label="S-W mismatch score (--mismatch)" />
        <param name="sw_gap_open_ref" type="integer" value="-33" optional="true" label="S-W gap open reference penalty (--open-r)" />
        <param name="sw_gap_open_query" type="integer" value="-33" optional="true" label="S-W gap open query penalty (--open-q)" />
        <param name="sw_gap_ext_ref" type="integer" value="-7" optional="true" label="S-W gap extend reference penalty (--ext-r)" />
        <param name="sw_gap_ext_query" type="integer" value="-3" optional="true" label="S-W gap extend query penalty (--ext-q)" />
        <param name="sw_hit_threshold" type="float" value="50.0" optional="true" label="S-W hit threshold (--full-threshold)" help="In percentage" />
        <param name="cmw_threshold" type="float" value="55.0" optional="true" label="Window generation threshold (-r)" help="In percentage" />
        <param name="cmw_overlap" type="float" value="90.0" optional="true" label="Match window overlap length (--cmw-overlap)" help="In percentage" />
        <param name="longest_read" type="integer" value="1000" optional="true" label="Maximum read length (--longest-read)" />
        <param name="insert_size_dist" type="text" value="200,100" label="Specifies the mean and standard deviation of the insert sizes (--insert-size-dist)" help="Format is &quot;mean,stddev&quot;" />
        <param name="read_group" type="text" value="" label="Include in the SAM output the read group and the pool name (--read-group)" help="Format is &quot;read_group,pool_name&quot;" />
        <param name="single_best_mapping" type="boolean" truevalue="--single-best-mapping" falsevalue="" checked="false" label="Report only the best mapping(s), this is not strata (--single-best-mapping)" />
        <param name="no_mapping_qualities" type="boolean" truevalue="--no-mapping-qualities" falsevalue="" checked="false" label="Do not compute mapping qualities (--no-mapping-qualities)" />
        <param name="strata" type="boolean" truevalue="--strata" falsevalue="" checked="false" label="Print only the best scoring hits (--strata)" />
<!--        <param name="trim_front" type="integer" value="0" optional="true" label="Trim front of reads by this amount (- -trim-front)" />
        <param name="trim_end" type="integer" value="0" optional="true" label="Trim end of reads by this amount (- -trim-end)" /> Trimming of paired reads causes segmentation fault in SHRiMP 2.2.3 -->
        <param name="min_avg_qv" type="integer" value="10" optional="true" label="The minimum average quality value of a read (--min-avg-qv)" />
        <param name="trim_off" type="boolean" truevalue="--trim-off" falsevalue="" checked="false" label="Disable automatic genome index trimming (--trim-off)" />
      </when>
    </conditional>
  </inputs>
  <outputs>
    <data name="output" format="sam" label="${tool.name} on ${on_string}: SAM" />
    <data name="log" format="txt" label="${tool.name} on ${on_string}: log" />
  </outputs>
  <tests>
  </tests>
  <help>
.. class:: warningmark

IMPORTANT: This tool currently only supports data where the quality scores are integers or ASCII quality scores, i.e. fastqsanger, fastqsolexa or fastqillumina datatypes.

-----

**What it does**

SHRiMP (SHort Read Mapping Package) is a software package for aligning genomic reads against a target genome.

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `SHRiMP`_.

.. _SHRiMP: http://compbio.cs.toronto.edu/shrimp/

If you use this tool, please cite:

- |Cuccuru2014|_
- |David2011|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |David2011| replace:: David, M., Dzamba, M., Lister, D., Ilie, L., Brudno, M. (2011) SHRiMP2: Sensitive yet Practical Short Read Mapping. *Bioinformatics* 27(7), 1011-1012
.. _David2011: http://bioinformatics.oxfordjournals.org/content/27/7/1011
  </help>
</tool>
